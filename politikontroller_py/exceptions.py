

class NoAccessException(Exception):
    pass


class AuthenticationError(Exception):
    pass
